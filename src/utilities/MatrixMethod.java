package utilities;
public class MatrixMethod {
    public static int[][] duplicate(int[][] initArray){
        int[][] duplicatedArray = new int[initArray.length][initArray[1].length*2];
        for (int i=0; i<initArray.length;i++){
            for (int j = 0; j <initArray[i].length ; j++) {
                duplicatedArray[i][j*2]=initArray[i][j];
                duplicatedArray[i][j*2+1]=initArray[i][j];
            }
        }
        return duplicatedArray;
    }
}
