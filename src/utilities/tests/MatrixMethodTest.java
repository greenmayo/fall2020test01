package utilities.tests;

import org.junit.jupiter.api.Test;
import utilities.MatrixMethod;

import static org.junit.jupiter.api.Assertions.*;


class MatrixMethodTest {

    @Test
    void testDuplicate() {
        int[][] a = {{1,2,3},{4,5,6}};
        int[][] control = {{ 1,1, 2, 2, 3, 3},{4,4,5,5,6,6 }};
        int[][] b = MatrixMethod.duplicate(a);
        for(int i=0; i<b.length; i++) {
            assertArrayEquals(control[i], b[i]);
        }
    }
}