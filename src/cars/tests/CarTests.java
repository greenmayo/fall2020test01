package cars.tests;
import cars.Car;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarTests {
    Car tesla = new Car(10);
    @Test
    public void testSpeedValidation() {
        try {
            Car buick = new Car(-1); // should throw an exception
            fail("Car was supposed to throw an exception but did not");
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
            fail("Car threw an exception, but it was not what expected");
        }
    }

        @Test
    void getSpeed() {
        assertEquals(10,tesla.getSpeed());
    }

    @Test
    void getLocation() {
        assertEquals(50, tesla.getLocation());
    }

    @Test
    void moveRight() {
        tesla.moveRight();
        assertEquals(60, tesla.getLocation());
    }

    @Test
    void moveLeft() {
        tesla.moveLeft();
        tesla.moveLeft();
        assertEquals(30, tesla.getLocation());
    }

    @Test
    void accelerate() {
        tesla.accelerate();
        assertEquals(11, tesla.getSpeed());
    }

    @Test
    void stop() {
        tesla.stop();
        assertEquals(0, tesla.getSpeed());
    }
}